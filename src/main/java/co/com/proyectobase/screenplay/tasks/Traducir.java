package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.GoogleHomePage;
import co.com.proyectobase.screenplay.ui.GoogleTraductorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Traducir implements Task{
	private String palabra;
	
	public Traducir(String palabra) {
		super();
		this.palabra = palabra;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(GoogleHomePage.BOTON_APLICACIONES));
		actor.attemptsTo(Click.on(GoogleHomePage.BOTON_GOOGLE_TRANSLATE));
		
		actor.attemptsTo(Click.on(GoogleTraductorPage.BOTON_IDIOMA_ORIGEN));
		actor.attemptsTo(Click.on(GoogleTraductorPage.OPCION_INGLES));
		actor.attemptsTo(Click.on(GoogleTraductorPage.BOTON_IDIOMA_DESTINO));
		actor.attemptsTo(Click.on(GoogleTraductorPage.OPCION_ESPANOL));
		actor.attemptsTo(Enter.theValue(palabra).into(GoogleTraductorPage.TEXTAREA_TRADUCIR));
		
		actor.attemptsTo(Click.on(GoogleTraductorPage.BOTON_TRADUCIR));
	}

	public static Traducir DelInglesAEspanolLa(String palabra) {
		return Tasks.instrumented(Traducir.class, palabra);
	}

}
