package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.OrangeHrmLoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class OrangeHrmIngresar implements Task{
	
	OrangeHrmLoginPage orangeHrmLoginPage; 

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(orangeHrmLoginPage));
	}

	public static OrangeHrmIngresar alLoginDeOrangeHrm() {
		return Tasks.instrumented(OrangeHrmIngresar.class);
	}

}
