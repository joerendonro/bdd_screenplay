package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.exceptions.NombrePorDefectoNoEncontradoException;
import static co.com.proyectobase.screenplay.exceptions.NombrePorDefectoNoEncontradoException.PRIMER_NOMBRE_INVALIDO;
import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.ui.AutomationDemoSiteRegisterPage;
import cucumber.api.DataTable;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.containsText;
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;

public class Registrar implements Task{
	
	private String nombre;
	private String apellido;
	private String direccion;
	private String email;
	private String telefono;
	private String genero;
	private String hobbies;
	private String idioma;
	private String skills;
	private String pais;
	private String anho;
	private String mes;
	private String dia;
	private String password;
	private String confirmPass;
	
	
	public Registrar(DataTable userData, int pos) {
		super();
		List<List<String>> dataUser = userData.raw();
		
		this.nombre 	= dataUser.get(pos).get(0).trim();
		this.apellido 	= dataUser.get(pos).get(1).trim();
		this.direccion 	= dataUser.get(pos).get(2).trim();
		this.email		= dataUser.get(pos).get(3).trim();
		this.telefono 	= dataUser.get(pos).get(4).trim();
		this.genero 	= dataUser.get(pos).get(5).trim();
		this.hobbies 	= dataUser.get(pos).get(6).trim();
		this.idioma 	= dataUser.get(pos).get(7).trim();
		this.skills 	= dataUser.get(pos).get(8).trim();
		this.pais 		= dataUser.get(pos).get(9).trim();
		this.anho 		= dataUser.get(pos).get(10).trim();
		this.mes 		= dataUser.get(pos).get(11).trim();
		this.dia 		= dataUser.get(pos).get(12).trim();
		this.password 	= dataUser.get(pos).get(13).trim();
		this.confirmPass= dataUser.get(pos).get(14).trim();

	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.should(seeThat(the(AutomationDemoSiteRegisterPage.TXT_FIRST_NAME), containsText("Andreina"))
				.orComplainWith(NombrePorDefectoNoEncontradoException.class, PRIMER_NOMBRE_INVALIDO));
		
		actor.attemptsTo(Enter.theValue(nombre).into(AutomationDemoSiteRegisterPage.TXT_FIRST_NAME));
		actor.attemptsTo(Enter.theValue(apellido).into(AutomationDemoSiteRegisterPage.TXT_LAST_NAME));
		actor.attemptsTo(Enter.theValue(direccion).into(AutomationDemoSiteRegisterPage.TXT_ADDRESS));
		actor.attemptsTo(Enter.theValue(email).into(AutomationDemoSiteRegisterPage.TXT_EMAIL));
		actor.attemptsTo(Enter.theValue(telefono).into(AutomationDemoSiteRegisterPage.TXT_TELEFONO));
		if(genero.equalsIgnoreCase("Male")) {
			actor.attemptsTo(Click.on(AutomationDemoSiteRegisterPage.RADBUTON_MASCULINO));
		}else if(genero.equalsIgnoreCase("FeMale")) {
			actor.attemptsTo(Click.on(AutomationDemoSiteRegisterPage.RADBUTON_FEMENINO));
		}
		if(hobbies.equalsIgnoreCase("Movies")) {
			actor.attemptsTo(Click.on(AutomationDemoSiteRegisterPage.CHECK_MOVIES));
		}
		actor.attemptsTo(Click.on(AutomationDemoSiteRegisterPage.TXT_LANGUAGE));
		actor.attemptsTo(Esperar.aMoment(3000));				
		actor.attemptsTo(Click.on("//a[@class='ui-corner-all'][text()='" + idioma + "']"));
		actor.attemptsTo(Click.on("//LABEL[@class='col-md-3 col-xs-3 col-sm-3 control-label'][text()='Languages']"));
		
		actor.attemptsTo(SelectFromOptions.byVisibleText(skills).from(AutomationDemoSiteRegisterPage.SEL_SKILLS));
		actor.attemptsTo(SelectFromOptions.byVisibleText(pais).from(AutomationDemoSiteRegisterPage.SEL_PAIS));
		actor.attemptsTo(SelectFromOptions.byVisibleText(anho).from(AutomationDemoSiteRegisterPage.SEL_ANHO));
		actor.attemptsTo(SelectFromOptions.byVisibleText(mes).from(AutomationDemoSiteRegisterPage.SEL_MES));
		actor.attemptsTo(SelectFromOptions.byVisibleText(dia).from(AutomationDemoSiteRegisterPage.SEL_DIA));
		
		actor.attemptsTo(Enter.theValue(password).into(AutomationDemoSiteRegisterPage.TXT_PASSWORD));
		actor.attemptsTo(Enter.theValue(confirmPass).into(AutomationDemoSiteRegisterPage.TXT_CONFIRM_PASSWORD));
		
		actor.attemptsTo(Click.on(AutomationDemoSiteRegisterPage.BTN_REGISTRAR));
		
	}

	public static Registrar elUsuario(DataTable dataUser, int posFila) {
		return Tasks.instrumented(Registrar.class, dataUser, posFila);
	}

}
