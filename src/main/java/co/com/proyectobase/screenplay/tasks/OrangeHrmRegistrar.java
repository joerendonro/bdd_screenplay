package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.ui.OrangeHrmAddEmployeePage;
import co.com.proyectobase.screenplay.ui.OrangeHrmLoginPage;
import co.com.proyectobase.screenplay.ui.OrangeHrmMenuPimPage;
import co.com.proyectobase.screenplay.ui.OrangeHrmPersonalDetailsPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

public class OrangeHrmRegistrar implements Task{
	
	private List<List<String>> userData;
	private int posFila;
	
	public OrangeHrmRegistrar(List<List<String>> data, int posFila) {
		userData = data;
		this.posFila = posFila;
	}


	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(OrangeHrmLoginPage.BTN_LOGIN));
		
		actor.attemptsTo(Click.on(OrangeHrmMenuPimPage.PIM));
		actor.attemptsTo(Click.on(OrangeHrmMenuPimPage.ADD_EMPLOYEE));
		
		actor.attemptsTo(Esperar.aMoment(4000));
		
		//Realizar Registro - Información básica
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(0)).into(OrangeHrmAddEmployeePage.TXT_NOMBRE));
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(1)).into(OrangeHrmAddEmployeePage.TXT_SEGUNDO_NOMBRE));
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(2)).into(OrangeHrmAddEmployeePage.TXT_APELLIDO));
		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.SEL_LOCATION));
		actor.attemptsTo(Click.on("//span[contains(text(), '" +userData.get(posFila).get(3)+ "')]"));//Seleccionar ubicación
		actor.attemptsTo(Click.on(OrangeHrmAddEmployeePage.BTN_GUARDAR));

		actor.attemptsTo(Esperar.aMoment(3000));
		
		//Completar registro - Información detallada
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(4)).into(OrangeHrmPersonalDetailsPage.TXT_OTHER_ID));
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(5)).into(OrangeHrmPersonalDetailsPage.TXT_FECHA_NACIMIENTO));
		actor.attemptsTo(Click.on(OrangeHrmPersonalDetailsPage.SEL_MARITAL_STATUS));
		actor.attemptsTo(Esperar.aMoment(300));
		actor.attemptsTo(Click.on("//span[contains(text(), '"+userData.get(posFila).get(6)+"')]"));//Seleccionar MARITAL_STATUS 
		
		String genero = userData.get(posFila).get(7);
		if(genero.equalsIgnoreCase("Male")) 
			actor.attemptsTo(Click.on(OrangeHrmPersonalDetailsPage.CHECK_MASCULINO));
		else
			actor.attemptsTo(Click.on(OrangeHrmPersonalDetailsPage.CHECK_FEMENINO));
		
		actor.attemptsTo(Click.on(OrangeHrmPersonalDetailsPage.SEL_NATIONALITY));
		actor.attemptsTo(Esperar.aMoment(300));
		actor.attemptsTo(Click.on("//SPAN[text()='"+userData.get(posFila).get(8)+"']"));//Seleccionar NATIONALITY
		
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(9)).into(OrangeHrmPersonalDetailsPage.TXT_LICENSE_NUMBER));
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(10)).into(OrangeHrmPersonalDetailsPage.TXT_LICENSE_EXPIRY_DATE));
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(11)).into(OrangeHrmPersonalDetailsPage.TXT_NICK_NAME));
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(12)).into(OrangeHrmPersonalDetailsPage.TXT_MILITAR_SERVICE));
		actor.attemptsTo(Click.on(OrangeHrmPersonalDetailsPage.BTN_GUARDAR_INFO_DETALLADA));
		
		//Completar registro - Información Importante
		actor.attemptsTo(Scroll.to(OrangeHrmPersonalDetailsPage.TXT_HOBBIES));
		actor.attemptsTo(Click.on(OrangeHrmPersonalDetailsPage.SEL_BLOOD_GROUP));
		actor.attemptsTo(Click.on("//span[contains(text(),'"+userData.get(posFila).get(13)+"')]"));//Seleccionar grupo sanguineo
		actor.attemptsTo(Esperar.aMoment(300));
		actor.attemptsTo(Enter.theValue(userData.get(posFila).get(14)).into(OrangeHrmPersonalDetailsPage.TXT_HOBBIES));
		actor.attemptsTo(Click.on(OrangeHrmPersonalDetailsPage.BTN_GUARDAR_INFO_IMPORTANTE));
		
		actor.attemptsTo(Esperar.aMoment(1000));
						
	}
	
	public static OrangeHrmRegistrar elUsuario(List<List<String>> dataUser, int posFila) {
		return Tasks.instrumented(OrangeHrmRegistrar.class, dataUser, posFila);
	}

}
