package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.AutomationDemoSiteRegisterPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Acceder implements Task{
	
	private AutomationDemoSiteRegisterPage automationDemoSiteRegisterPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(automationDemoSiteRegisterPage));	
	}

	public static Acceder aLaPaginaDemo() {
		return Tasks.instrumented(Acceder.class);
	}
}
