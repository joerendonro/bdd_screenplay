package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.Esperar;
import co.com.proyectobase.screenplay.ui.OrangeHrmAddEmployeePage;
import co.com.proyectobase.screenplay.ui.OrangeHrmEmployeeListPage;
import co.com.proyectobase.screenplay.ui.OrangeHrmMenuPimPage;
import gherkin.ast.ScenarioOutline;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

public class OrangeHrmBuscar implements Task{
	
	private String nombreEmpleado;
	
	public OrangeHrmBuscar(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;

	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		//actor.attemptsTo(Click.on(OrangeHrmMenuPimPage.PIM));
		//actor.attemptsTo(Esperar.aMoment(1000));
		actor.attemptsTo(Scroll.to(OrangeHrmMenuPimPage.EMPLOYEE_LIST));
		actor.attemptsTo(Click.on(OrangeHrmMenuPimPage.EMPLOYEE_LIST));
		actor.attemptsTo(Esperar.aMoment(2000));
		
		actor.attemptsTo(Enter.theValue(nombreEmpleado).into(OrangeHrmEmployeeListPage.TXT_SEARCH_EMPLOYEE_NAME));
		actor.attemptsTo(Click.on(OrangeHrmEmployeeListPage.BTN_SEARCH_EMPLOYEE_NAME));
		actor.attemptsTo(Esperar.aMoment(2000));
		
	}

	public static OrangeHrmBuscar empleadoConElNombre(String nameUser) {
		return Tasks.instrumented(OrangeHrmBuscar.class, nameUser);
	}

}
