package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class GoogleTraductorPage extends PageObject{
	
	public static final Target BOTON_IDIOMA_ORIGEN = 
			Target.the("Botón de idioma origen").located(By.id("gt-sl-gms"));
	public static final Target BOTON_IDIOMA_DESTINO =
			Target.the("Botón de idioma destino").located(By.id("gt-tl-gms"));
	public static final Target OPCION_INGLES =
			Target.the("Opción de idioma Inglés").located(By.id(":1e"));
	public static final Target OPCION_ESPANOL =
			Target.the("Opción de idioma Español").located(By.id(":3q"));
	public static final Target TEXTAREA_TRADUCIR =
			Target.the("Campo donde se ingresan las palabras a traducir").located(By.id("source"));
	public static final Target BOTON_TRADUCIR =
			Target.the("El botón para traducir").located(By.id("gt-submit"));
	public static final Target TEXTAREA_TRADUCIDA =
			Target.the("Área donde se muestran las palabras traducidas").located(By.id("gt-res-dir-ctr"));
	
}
