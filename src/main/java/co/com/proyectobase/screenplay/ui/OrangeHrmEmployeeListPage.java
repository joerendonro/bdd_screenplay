package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class OrangeHrmEmployeeListPage extends PageObject{
	public static final Target TXT_SEARCH_EMPLOYEE_NAME	= Target.the("Ingresar el nombre de búsqueda").located(By.id("employee_name_quick_filter_employee_list_value"));
	public static final Target BTN_SEARCH_EMPLOYEE_NAME	= Target.the("Botón buscar").located(By.id("quick_search_icon"));
	public static final Target NOTIFICACION_NO_HAY_REGISTROS = Target.the("Otra identificación").located(By.xpath("//DIV[@class='toast-message'][text()='No Records Found']"));
	
	public static final Target RESULT_NOMBRE = Target.the("El nombre del empleado de la primera fila ").located(By.xpath("(//TD[@ng-click='vm.viewProfileIfHasAccessablePimTabs(employee)'])[3]"));
	
	public static final Target COLUMNA_NOMBRES = Target.the("La columna name de la tabla de resultados").located(By.xpath("//td[@ng-click='vm.viewProfileIfHasAccessablePimTabs(employee)'][3]"));
}

