package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class OrangeHrmMenuPimPage extends PageObject {
	
	public static final Target PIM = Target.the("Módulo de Información Personal")
			.located(By.id("menu_pim_viewPimModule"));
	
	public static final Target ADD_EMPLOYEE = Target.the("Opción para agregar empleado")
			.located(By.id("menu_pim_addEmployee"));
	
	public static final Target EMPLOYEE_LIST = Target.the("Opción para listar todos los empleados")
			.located(By.xpath("//SPAN[text()='Employee List']"));
}
