package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class OrangeHrmLoginPage extends PageObject{
	
	public static final Target BTN_LOGIN = Target.the("El botón de login").located(By.id("btnLogin"));	
	
}
