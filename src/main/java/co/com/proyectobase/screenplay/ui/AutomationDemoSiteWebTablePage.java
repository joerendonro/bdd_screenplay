package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class AutomationDemoSiteWebTablePage extends PageObject{
	
	public static final Target LABEL_REGISTRO_EXITOSO = Target.the("Titulo de la pestaña WebTable para verificar el registro exitoso.")
			.located(By.xpath("(//H4)[1]"));
}
