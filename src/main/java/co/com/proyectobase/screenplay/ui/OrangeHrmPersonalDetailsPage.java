package co.com.proyectobase.screenplay.ui;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class OrangeHrmPersonalDetailsPage extends PageObject{
	
	public static final Target TXT_OTHER_ID 		= Target.the("Otra identificación").located(By.id("other_id"));
	public static final Target TXT_FECHA_NACIMIENTO = Target.the("Fecha de nacimiento").located(By.id("date_of_birth"));
	//MARITAL_STATUS 	 //SPAN[text()='Single']
	public static final Target SEL_MARITAL_STATUS 	= Target.the("Estado civil").located(By.id("marital_status_inputfileddiv"));
	public static final Target CHECK_MASCULINO 		= Target.the("Genero masculino").located(By.xpath("(//li[@ng-repeat=\"item in form.titleMap\"])[1]"));
	public static final Target CHECK_FEMENINO 		= Target.the("Genero femenino").located(By.xpath("(//li[@ng-repeat=\\\"item in form.titleMap\\\"])[2]"));
	//NATIONALITY		
	public static final Target SEL_NATIONALITY 		= Target.the("Nacionalidad").located(By.id("nationality_inputfileddiv"));
	public static final Target TXT_LICENSE_NUMBER 	= Target.the("Numero de la licencia").located(By.id("driver_license"));
	public static final Target TXT_LICENSE_EXPIRY_DATE = Target.the("Fecha de vencimiento de la licencia").located(By.id("license_expiry_date"));
	public static final Target TXT_NICK_NAME 		= Target.the("Apodo").located(By.id("nickName"));
	public static final Target TXT_MILITAR_SERVICE 	= Target.the("Servicio militar").located(By.id("militaryService"));
	public static final Target BTN_GUARDAR_INFO_DETALLADA = Target.the("Botón de guardar información personal detallada")
			.located(By.xpath("//BUTTON[@type='submit'][text()='Save']"));
	
	//BLOOD GROUP		////SPAN[text()='AB']
	public static final Target SEL_BLOOD_GROUP 			= Target.the("Tipo de sangre").located(By.id("1_inputfileddiv"));//
	public static final Target SEL_OPTION_BLOOD_GROUP 	= Target.the("Opción de tipo de sangre").located(By.xpath("//div[@id='1_inputfileddiv']//ul[contains(@id, 'select-options-')]"));// 
	public static final Target TXT_HOBBIES 				= Target.the("ingresar hobbies del cliente")
			.located(By.xpath("//INPUT[@id='5']"));
	public static final Target BTN_GUARDAR_INFO_IMPORTANTE = Target.the("Botón de guardar información importante")
			.located(By.xpath("//BUTTON[@type='submit'][text()='save']"));
	
	
	
	
}
