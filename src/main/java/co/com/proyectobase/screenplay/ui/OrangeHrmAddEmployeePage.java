package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class OrangeHrmAddEmployeePage extends PageObject{
	
	public static final Target TXT_NOMBRE = Target.the("Ingresar el nombre del empleado")
			.located(By.id("firstName"));
	
	public static final Target TXT_SEGUNDO_NOMBRE = Target.the("Ingresar el segundo nombre del empleado")
			.located(By.id("middleName"));
	
	public static final Target TXT_APELLIDO = Target.the("Ingresar el apellido del empleado")
			.located(By.id("lastName"));
	
	public static final Target SEL_LOCATION = Target.the("Click en el campo de selección de la ubucación del empleado")
			.located(By.id("location_inputfileddiv"));
	
	public static final Target BTN_GUARDAR = Target.the("Botón guardar registro del empleado.")
			.located(By.id("systemUserSaveBtn"));

	
}
