package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://demo.automationtesting.in/Register.html")
public class AutomationDemoSiteRegisterPage extends PageObject{
	public static final Target TXT_FIRST_NAME = Target.the("Ingresar el nombre")
			.located(By.xpath("//input[@placeholder='First Name']"));
	
	public static final Target TXT_LAST_NAME = Target.the("Ingresar el apellido")
			.located(By.xpath("//input[@placeholder='Last Name']"));
	
	public static final Target TXT_ADDRESS = Target.the("Ingresar la dirección")
			.located(By.xpath("//TEXTAREA[@rows='3']"));
	
	public static final Target TXT_EMAIL = Target.the("Ingresar el correo")
			.located(By.xpath("//INPUT[@type='email']"));
	
	public static final Target TXT_TELEFONO = Target.the("Ingrear el numero de teléfono")
			.located(By.xpath("//INPUT[@type='tel']"));
	
	public static final Target RADBUTON_MASCULINO = Target.the("Seleccionar sexo mascúlino")
			.located(By.xpath("//input[@value='Male']"));
	
	public static final Target RADBUTON_FEMENINO = Target.the("Seleccionar sexo femenino")
			.located(By.xpath("//input[@value='FeMale']"));
	
	public static final Target CHECK_MOVIES = Target.the("Seleccionar el hobbie movies")
			.located(By.id("checkbox2"));
	
	public static final Target TXT_LANGUAGE = Target.the("Ingresar el idioma")
			.located(By.id("msdd"));

	public static final Target SEL_SKILLS = Target.the("Seleccionar habilidades")
			.located(By.id("Skills"));
	
	public static final Target SEL_PAIS = Target.the("Seleccionar el país")
			.located(By.id("countries"));
	
	public static final Target SEL_ANHO = Target.the("Seleccionar el año de nacimiento")
			.located(By.id("yearbox"));
	
	public static final Target SEL_MES = Target.the("Seleccionar el mes de nacimiento")
			.located(By.xpath("//SELECT[@placeholder='Month']"));
	
	public static final Target SEL_DIA = Target.the("Seleccionar el día de nacimiento")
			.located(By.id("daybox"));
	
	public static final Target TXT_PASSWORD = Target.the("Ingrear la contraseña")
			.located(By.id("firstpassword"));
	
	public static final Target TXT_CONFIRM_PASSWORD = Target.the("Confirmar la contraseña")
			.located(By.id("secondpassword"));
	
	public static final Target BTN_REGISTRAR = Target.the("Botón para crear el registro")
			.located(By.id("submitbtn"));
	
	public void esperar(int tiempo) {
		waitABit(tiempo);
	}
	
}
