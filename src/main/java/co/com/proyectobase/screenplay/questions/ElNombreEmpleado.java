package co.com.proyectobase.screenplay.questions;

import java.util.List;

import co.com.proyectobase.screenplay.ui.OrangeHrmEmployeeListPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

/**
 * Esta clase es tipo List para poder obtener varios resultados de un xpath y realizar la comparación en el step definition
 * @author jrendonr
 *
 */
public class ElNombreEmpleado implements Question<List<String>>{

	@Override
	public List<String> answeredBy(Actor actor) {
		return Text.of(OrangeHrmEmployeeListPage.COLUMNA_NOMBRES)
				.viewedBy(actor)
				.asList();
	}

	public static ElNombreEmpleado es() {
		return new ElNombreEmpleado();
	}

}
