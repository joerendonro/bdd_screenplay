package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.ui.AutomationDemoSiteWebTablePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ElTexto implements Question<String>{

	public static ElTexto es() {
		return new ElTexto();
	}

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(AutomationDemoSiteWebTablePage.LABEL_REGISTRO_EXITOSO).viewedBy(actor).asString();
	}

}
