package co.com.proyectobase.screenplay.exceptions;

public class NombrePorDefectoNoEncontradoException extends AssertionError{
	
	public static final String PRIMER_NOMBRE_INVALIDO = "El Target no contiene el valor esperado.";

	public NombrePorDefectoNoEncontradoException(String mensaje, Throwable causa) {
		super(mensaje, causa);
		
	}
	

}
