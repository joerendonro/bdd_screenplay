package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;


import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefintions {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor john	= Actor.named("John");
	
	@Before
	public void configuracionInicial() {
		john.can(BrowseTheWeb.with(hisBrowser));
	}
	
	
	@Dado("^que John quiere usar el traductor de Google$")
	public void queJohnQuiereUsarElTraductorDeGoogle() throws Exception {
		john.wasAbleTo(Abrir.LaPaginaDeGoogle());
	}

	@Cuando("^el traduce la palabra \"([^\"]*)\" del Inglés a Español$")
	public void elTraduceLaPalabraTableDelInglésAEspañol(String palabra) throws Exception {
		john.attemptsTo(Traducir.DelInglesAEspanolLa(palabra));
	}

	@Entonces("^el deberia ver la palabra \"([^\"]*)\" en pantalla$")
	public void elDeberiaVerLaPalabraMesaEnPantallaL(String pregunta) throws Exception {
		john.should(seeThat(LaRespuesta.es(), equalTo(pregunta)));
	}
}
