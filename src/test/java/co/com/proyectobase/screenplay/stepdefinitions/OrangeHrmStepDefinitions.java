package co.com.proyectobase.screenplay.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;

import java.util.List;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.ElNombreEmpleado;
import co.com.proyectobase.screenplay.questions.ElTexto;
import co.com.proyectobase.screenplay.tasks.OrangeHrmBuscar;
import co.com.proyectobase.screenplay.tasks.OrangeHrmIngresar;
import co.com.proyectobase.screenplay.tasks.OrangeHrmRegistrar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class OrangeHrmStepDefinitions {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor john = Actor.named("John");
	
	@Before
	public void configInit() {
		john.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Dado("^que John necesita crear un empleado en el OrageHRM$")
	public void queJohnNecesitaCrearUnEmpleadoEnElOrageHRM(){
		john.wasAbleTo(OrangeHrmIngresar.alLoginDeOrangeHrm());
	}
	
	@Cuando("^el realiza el ingreso del registro en la aplicación$")
	public void elRealizaElIngresoDelRegistroEnLaApp(DataTable data) {
		List<List<String>> dataUser = data.raw();
		john.attemptsTo(OrangeHrmRegistrar.elUsuario(dataUser, 0));
	}
	
	@Entonces("^el visualiza el nuevo empleado en el aplicativo$")
	public void verificarLaCreacionDelEmpleado(DataTable data) {
		List<List<String>> nameUser = data.raw();
		String primerNombreEmpleado = nameUser.get(0).get(0);
		String nombreCompletoEmpleado = nameUser.get(0).get(0)+" "+nameUser.get(0).get(1)+" "+nameUser.get(0).get(2);
		john.attemptsTo(OrangeHrmBuscar.empleadoConElNombre(primerNombreEmpleado));
		
		//Se obtiene una lista de resultados que coinciden con el primer nombre del empleado
		//y se compara con el nombre completo del empleado
		john.should(seeThat(ElNombreEmpleado.es(), hasItem(nombreCompletoEmpleado)));
	}
}
