package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.ElTexto;
import co.com.proyectobase.screenplay.tasks.Acceder;
import co.com.proyectobase.screenplay.tasks.Registrar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class AutomationDemoSiteStepDefinitions {
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor elkin = Actor.named("Elkin");
	
	@Before
	public void configInit() {
		elkin.can(BrowseTheWeb.with(hisBrowser));
		System.out.println();
	}
	
	
	@Dado("^que Elkin quiere acceder a la Web Automation Demo Site$")
	public void queElkinQuiereAccederALaWebAutomationDemoSite() throws Exception {
		elkin.wasAbleTo(Acceder.aLaPaginaDemo());
	}


	@Cuando("^el realiza el registro en la página$")
	public void elRealizaElRegistroEnLaPágina(DataTable dataUser) throws Exception {
		elkin.attemptsTo(Registrar.elUsuario(dataUser, 1));
	}

	@Entonces("^el verifica que se carga la pantalla con texto (.*)$")
	public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow(String textoEsperado) throws Exception {
		elkin.should(seeThat(ElTexto.es(), equalTo(textoEsperado)));
	}
}
