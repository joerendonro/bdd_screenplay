#Author: jrendonr@choucairtesting.com
# language:es
@automationDemoSite
Característica: sitio Automation Demo Site
	Cómo usuario quiero ingresar al sitio
	para registrarme y poder acceder.

  @registro
  Escenario: Registrarse en Automation Demo Site
    Dado que Elkin quiere acceder a la Web Automation Demo Site
    Cuando el realiza el registro en la página
    | Nombre  		| Apellido | Direccion  |Email  										 |Telefono  |Genero  |Hobbies|idioma |Skills|Pais		 |año |mes|dia|pasword |confirmPass|
    | John Elkin 	| Rendon R | Medellin  	|jrendonr@choucairtesting.com|3163154784|Male		 |Movies |Spanish|Java 	|Colombia|1992|September |13 |Demo1234|Demo1234		|
    Entonces el verifica que se carga la pantalla con texto - Double Click on Edit Icon to EDIT the Table Row.

