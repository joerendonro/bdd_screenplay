#Author: jrendonr@choucairtesting.com
# language:es
@OrangeHrmDemo
Característica: trabajar con el sitio web Orange HRM Demo
  Realizar registro de empleados

  @Registrar
  Esquema del escenario: Registrar un empleado en el sitio
    Dado que John necesita crear un empleado en el OrageHRM
    Cuando el realiza el ingreso del registro en la aplicación
      | <nombre1> | <nombre2> | <apellido> | <ubicacion> | <other_id> | <date_birth> | <Marital_status> | <Gender> | <Nationality> | <License_number> | <license_expiry> | <NickName> | <MilitarService> | <BloodGroup> | <Hobbies> | <EmployeeID> |
    Entonces el visualiza el nuevo empleado en el aplicativo
      | <nombre1> | <nombre2> | <apellido> |

    Ejemplos: 
      | nombre1 | nombre2 | apellido | ubicacion     | other_id | date_birth | Marital_status | Gender | Nationality | License_number | license_expiry | NickName | MilitarService | BloodGroup | Hobbies     | EmployeeID |
      | John    | A.      | Castaño  | London Office |   108937 | 1992-09-13 | Single         | Male   | Colombian   |     3426891008 | 2022-11-29     | King     | Si             | AB         | Travel, eat |       0500 |
