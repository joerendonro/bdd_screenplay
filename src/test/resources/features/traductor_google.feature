#Author: jerendon@bancolombia.com.co
# language:es
Característica: Traductor de Google
  Como usuario
  Quiero ingresar al traductor de Google
  A traducir las palabras entre diferentes lenguajes

  Escenario: Traducir del Inglés al Español
    Dado que John quiere usar el traductor de Google
    Cuando el traduce la palabra "table" del Inglés a Español
    Entonces el deberia ver la palabra "mesa" en pantalla